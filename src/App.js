import React from 'react'; // node_modules配下にあるライブラリは名前だけ
import LoginForm from './login-form';
import ItemList from "./item-list"; // 自作したコンポーネントは相対パスで指定

function App() {
  return (
      <div>
        <ItemList />
      </div>
  );
}
export default App;
